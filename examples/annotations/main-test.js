/* globals $, geo, utils */

var annotationDebug = {};

// Run after the DOM loads
$(function() {
  "use strict";

//   var layer, fromButtonSelect, fromGeojsonUpdate;
//   // get the query parameters and set controls appropriately
//   var query = utils.getQuery();

//   var duomo = {
//     Image: {
//       Format: "jpeg",
//       Overlap: 1,
//       Size: {
//         Height: 36000,
//         Width: 46000
//       },
//       TileSize: 510,
//       Url:
//         "https://openslide-demo.s3.dualstack.us-east-1.amazonaws.com/aperio/cmu-1-jp2k-33005/slide_files/",
//       xmlns: "http://schemas.microsoft.com/deepzoom/2008"
//     }
//   };

//   var osd = OpenSeadragon({
//     id: "map",
//     prefixUrl: "openseadragon-images/",
//     tileSources: duomo,

//   });

//   const params = geo.util.pixelCoordinateParams("#map", 7026, 9221, 256, 256);
//   params.map.clampZoom = false;
//   params.map.clampBoundsX = false;
//   params.map.clampBoundsY = false;
//   const map = geo.map(params.map);
//   // // turn off geojs map navigation
//   map.interactor().options({ actions: [] });

//   function getBounds() {
//     return osd.viewport.viewportToImageRectangle(osd.viewport.getBounds(true));
//   }
//   function setBounds() {
//     const bounds = getBounds();
//     map.bounds({
//       left: bounds.x,
//       right: bounds.x + bounds.width,
//       top: bounds.y,
//       bottom: bounds.y + bounds.height
//     });
//   }
//   osd.addHandler("open", setBounds);
//   osd.addHandler("animation", setBounds);

//    // add a handler for when an annotation is created
//   function created(evt) {
//     $("#map .geojs-layer").css("pointer-events", "none");

//     // write out the annotation definition
//     console.log(evt.annotation.features()[0]);
//   }
//   map.geoOn(geo.event.annotation.state, created);

//   //  add handlers for drawing annotations
//   function draw(evt) {
//     $("#map .geojs-layer").css("pointer-events", "auto");
//     const type = $(evt.target).data("type");
//     layer.mode(type);
//   }
//   $(".controls-container button").on("click",draw);

  // the global image size and tiling
const height = 9221;
const width = 7026;
const tsize = 256;

// initialize the openseadragon viewer
var duomo = {
    Image: {
      Format: "jpeg",
      Overlap: 1,
      Size: {
        Height: 36000,
        Width: 46000
      },
      TileSize: 510,
      Url:
        "https://openslide-demo.s3.dualstack.us-east-1.amazonaws.com/aperio/cmu-1-jp2k-33005/slide_files/",
      xmlns: "http://schemas.microsoft.com/deepzoom/2008"
    }
  };

  var osd = OpenSeadragon({
    id: "map",
    prefixUrl: "openseadragon-images/",
    tileSources: duomo,

  });

// initialize the geojs viewer
const params = geo.util.pixelCoordinateParams('#map', width, height, tsize, tsize);
params.map.clampZoom = false;
params.map.clampBoundsX = false;
params.map.clampBoundsY = false;
const map = geo.map(params.map);
const layer = map.createLayer('annotation');

// turn off geojs map navigation
map.interactor().options({actions: []});

// get the current bounds from the osd viewer
function getBounds() {
    return osd.viewport.viewportToImageRectangle(osd.viewport.getBounds(true));
}

// set the geojs bounds from the osd bounds
function setBounds() {
    const bounds = getBounds();
    map.bounds({
        left: bounds.x,
        right: bounds.x + bounds.width,
        top: bounds.y,
        bottom: bounds.y + bounds.height
    });
}

// add handlers to tie navigation events together
osd.addHandler('open', setBounds);
osd.addHandler('animation', setBounds);

// add a handler for when an annotation is created
function created(evt) {
    $('#map .geojs-layer').css('pointer-events', 'none');
    
    // write out the annotation definition
    console.log(evt.annotation.features()[0]);
}
map.geoOn(geo.event.annotation.state, created);

// add handlers for drawing annotations
function draw(evt) {
    $('#map .geojs-layer').css('pointer-events', 'auto');
    const type = $(evt.target).data('type');
    layer.mode(type);
}
$(".controls-container button").on("click",draw);

  console.log(query);
  console.log(map);
});
